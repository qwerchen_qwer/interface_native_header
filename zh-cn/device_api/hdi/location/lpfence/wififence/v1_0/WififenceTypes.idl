/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup HdiLpfenceWififence
 * @{
 *
 * @brief 为低功耗围栏服务提供Wi-Fi围栏的API
 *
 * 本模块接口提供添加Wi-Fi围栏，删除Wi-Fi围栏，获取Wi-Fi围栏状态，获取Wi-Fi围栏使用信息的功能。
 * 应用场景：一般用于判断设备是否在室内特定位置，如居所内或商场的某个店铺内。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file WififenceTypes.idl
 *
 * @brief 定义Wi-Fi围栏使用的数据类型。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @brief Wi-Fi围栏模块接口的包路径。
 *
 * @since 4.0
 */
package ohos.hdi.location.lpfence.wififence.v1_0;

/**
 * @brief 枚举Wi-Fi围栏状态事件。
 *
 * @since 4.0
 */
enum WififenceTransition {
    /** 设备在Wi-Fi围栏范围内。 */
    WIFIFENCE_TRANSITION_ENTERED = (1 << 0),
    /** 设备在Wi-Fi围栏范围外。 */
    WIFIFENCE_TRANSITION_EXITED = (1 << 1),
};

/**
 * @brief 枚举Wi-Fi围栏的匹配算法。
 *
 * @since 4.0
 */
enum WififenceAlgoType {
    /** Wi-Fi围栏中MAC地址与设备扫描的MAC地址有交集时，即认为设备在围栏内。 */
    TYPE_ONE_BSSID = 1,
    /** Wi-Fi围栏中MAC地址和RSSI值与设备扫描的MAC地址和RSSI值相似度达到一定阈值时，即认为设备在围栏内。 */
    TYPE_FP_MATCH = 2,
};

/**
 * @brief 定义添加Wi-Fi围栏的数据结构。
 *
 * @since 4.0
 */
struct WififenceRequest {
    /** Wi-Fi围栏的ID号，用于标识某个Wi-Fi围栏，不可重复添加相同ID号的围栏。 */
    int wififenceId;
    /** Wi-Fi围栏的匹配算法。详见{@Link WififenceAlgoType}。 */
    int algoType;
    /** 若使用{@Link TYPE_ONE_BSSID}类型算法，则是多组Wi-Fi MAC地址。
    若使用{@Link TYPE_FP_MATCH}类型算法，则是多组的Wi-Fi MAC地址和对应RSSI值。 */
    unsigned char[] bssid;
};

/**
 * @brief 定义Wi-Fi围栏使用信息的数据结构。
 *
 * @since 4.0
 */
struct WififenceSize {
    /** 设备支持添加Wi-Fi围栏的最大个数。 */
    unsigned int maxNum;
    /** 设备当前已添加的Wi-Fi围栏个数。 */
    unsigned int usedNum;
};
/** @} */