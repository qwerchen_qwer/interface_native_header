/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup OH_Camera
 * @{
 *
 * @brief 为相机模块提供C接口的定义。
 *
 * @syscap SystemCapability.Multimedia.Camera.Core
 *
 * @since 11
 * @version 1.0
 */

/**
 * @file camera_input.h
 *
 * @brief 声明相机输入概念。
 *
 * @library libohcamera.so
 * @syscap SystemCapability.Multimedia.Camera.Core
 * @since 11
 * @version 1.0
 */

#ifndef NATIVE_INCLUDE_CAMERA_CAMERA_INPUT_H
#define NATIVE_INCLUDE_CAMERA_CAMERA_INPUT_H

#include <stdint.h>
#include <stdio.h>
#include "camera.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 相机输入对象。
 *
 * 可以使用{@link OH_CameraManager_CreateCameraInput}方法创建指针。
 *
 * @since 11
 * @version 1.0
 */
typedef struct Camera_Input Camera_Input;

/**
 * @brief 在{@link CameraInput_Callbacks}中被调用的相机输入错误回调。
 *
 * @param cameraInput 传递回调的{@link Camera_Input}。
 * @param errorCode 相机输入的{@link Camera_ErrorCode}。
 *
 * @see CAMERA_CONFLICT_CAMERA
 * @see CAMERA_DEVICE_DISABLED
 * @see CAMERA_DEVICE_PREEMPTED
 * @see CAMERA_SERVICE_FATAL_ERROR
 * @since 11
 */
typedef void (*OH_CameraInput_OnError)(const Camera_Input* cameraInput, Camera_ErrorCode errorCode);

/**
 * @brief 相机输入错误事件的回调。
 *
 * @see OH_CameraInput_RegisterCallback
 * @since 11
 * @version 1.0
 */
typedef struct CameraInput_Callbacks {
    /**
     * 相机输入错误事件。
     */
    OH_CameraInput_OnError onError;
} CameraInput_Callbacks;

/**
 * @brief 注册相机输入更改事件回调。
 *
 * @param cameraInput {@link Camera_Input}实例。
 * @param callback 要注册的{@link CameraInput_Callbacks}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 11
 */
Camera_ErrorCode OH_CameraInput_RegisterCallback(Camera_Input* cameraInput, CameraInput_Callbacks* callback);

/**
 * @brief 注销相机输入更改事件回调。
 *
 * @param cameraInput {@link Camera_Input}实例。
 * @param callback 要注销的{@link CameraInput_Callbacks}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 11
 */
Camera_ErrorCode OH_CameraInput_UnregisterCallback(Camera_Input* cameraInput, CameraInput_Callbacks* callback);

/**
 * @brief 打开相机。
 *
 * @param cameraInput 要打开的{@link Camera_Input}实例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_CONFLICT_CAMERA}如果不能使用相机会导致冲突。
 *         {@link#CAMERA_DEVICE_DISABLED}如果由于安全原因禁用了摄像头。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 11
 */
Camera_ErrorCode OH_CameraInput_Open(Camera_Input* cameraInput);

/**
 * @brief 关闭相机。
 *
 * @param cameraInput 要关闭的{@link Camera_Input}实例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 11
 */
Camera_ErrorCode OH_CameraInput_Close(Camera_Input* cameraInput);

/**
 * @brief 释放相机输入实例。
 *
 * @param cameraInput 要释放的{@link Camera_Input}实例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 11
 */
Camera_ErrorCode OH_CameraInput_Release(Camera_Input* cameraInput);

#ifdef __cplusplus
}
#endif

#endif // NATIVE_INCLUDE_CAMERA_CAMERA_INPUT_H
/** @} */